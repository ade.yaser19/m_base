<?php 

defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function update($data, $id)
    {
        $this->db->update('menu', $data, ['id' => $id]);
    }

    public function save($data)
    {
        $this->db->insert('menu', $data);
    }

}