<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
                        
        require APPPATH.'libraries/PHPMailer/src/Exception.php';
        require APPPATH.'libraries/PHPMailer/src/PHPMailer.php';
        require APPPATH.'libraries/PHPMailer/src/SMTP.php';
    }

    public function index()
    {
        $data['nama'] = $this->db->get('pengaturan')->row_array();
        $this->form_validation->set_rules('email', 'Email', 'trim|required', [
            'required'  =>  'Email tidak boleh kosong'
        ]);

        $this->form_validation->set_rules('password', 'Password', 'trim|required', [
            'required'  =>  'Kata sandi tidak boleh kosong'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/auth_head', $data);
            $this->load->view('auth/login', $data);
            // $this->load->view('templates/auth_foot');
        } else {
            $this->_logged();
        }
    }

    private function _logged()
    {
        $email      = $this->input->post('email');
        $password   = $this->input->post('password');

        $user = $this->db->get_where('users', ['email' => $email,'status_aktif'=>'1'])->row_array();

        if ($user) {
            if (password_verify($password, $user['password'])) {

                $role = $this->db->get_where('role', ['id' => $user['role_id']])->row_array();
                $data = [
                    'email'     => $email,
                    'role_id'   => $user['role_id'],
                    'user_id'   => $user['id'],
                    'role_name' => $role['role']
                ];
                $this->session->set_userdata($data);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Password anda salah
                    </div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Email anda tidak terdaftar atau belum dikonfirmasi
                </div>');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Anda telah keluar
            </div>');
        redirect('auth');
    }

    public function register()
    {

        $data['nama'] = $this->db->get('pengaturan')->row_array();
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required', [
            'required' => 'Nama anda tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]', [
            'required'     => 'Email tidak boleh kosong!',
            'valid_email'  => 'Email anda salah!',
            'is_unique'    => 'Email telah terdaftar'
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[5]', [
            'required'      => 'Kata sandi tidak boleh kosong',
            'min_length'    => 'Kata sandi anda terlalu pendek'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/auth_head', $data);
            $this->load->view('auth/register', $data);
            //$this->load->view('templates/auth_foot');
        } else {

            $alamat_email = htmlspecialchars($this->input->post('email'), TRUE);
            $param_key  = rand(10,1000);
            $param_date = date("hs");
            $param_key  = $param_key.$param_date;

            $data = [
                'role_id'       => '0',
                'nama'          => htmlspecialchars($this->input->post('nama'), TRUE),
                'email'         => $alamat_email ,
                'password'      => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'password_hint' => $this->input->post('password1'),
                'otp'           => $param_key,
                'img'           => 'default.png'
            ];
            $this->db->insert('users', $data);


            $link_aktifasi = base_url().'Confim_activation?key='.$param_key;

            $pesan ="<html>
                        <body>
                        <h2>Congratulations your registration has been successful </h2>
                        <p>
                        One step later, your account will be active
                        Click the confirmation link to activate</p>
                        <a href='".$link_aktifasi."'>confirm activation</a>
                        </body>
                    </html>";

            $this->sendemail($alamat_email,"Subject",$pesan);



            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Anda telah terdaftar, silahkan cek emai dan konfirmasi account untuk masuk ke portal
                </div>');
            redirect('auth');
        }
    }

    public function blocked()
    {
        $this->load->view('auth/blocked');
    }

    public function member()
    {
        $this->load->view('member/dashboard');
    }

    public function sendemail($to,$subject,$message){

        $email = $to;
        $judul = $subject;
        $pesan = $message;

        //Create an instance; passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 2;                      //Enable verbose debug output
            $mail->isSMTP();                           //Send using SMTP
            $mail->Host       = 'smtp.gmail.com';      //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                  //Enable SMTP authentication
            $mail->Username   = 'ade.yaser19@gmail.com';//SMTP username
            $mail->Password   = 'utbd unbh iuqh iidf';  //SMTP password
            $mail->SMTPSecure = 'tls';            //Enable implicit TLS encryption
            $mail->Port       = 587;              //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
            $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

            //pengirim
            $mail->setFrom('ade.yaser19@gmail.com', 'https://adey.web.id');
            $mail->addAddress($email);     //Add a recipient
         
            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = $judul;
            $mail->Body    = $pesan;
            $mail->AltBody = '';
            //$mail->AddEmbeddedImage('gambar/logo.png', 'logo'); //abaikan jika tidak ada logo
            //$mail->addAttachment(''); 

            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";

        }
                  //redirect ke halaman index.php
              //  echo "<script>alert('Email berhasil terkirim!');window.location='index.php';</script>";
    }  
}
