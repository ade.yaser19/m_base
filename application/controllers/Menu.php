<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('menu_model', 'menu');
    }

    public function index()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'Menu',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['menu'] = $this->db->get('menu')->result_array();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('menu/index');
        $this->load->view('templates/footer');
    }

    public function add()
    {   
        $post = $this->input->post();

        if ($post) {

            $data = [
                'title' => $post['title'],
                'icon' => $post['icon'],
                'url' => $post['url'],
            ];

            $this->menu->save($data);
            $this->session->set_flashdata('message', berhasil_tambah);
            redirect('menu');
        
        } else {

            $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
            $name           = $user['nama'];
            $img            = $user['img'];
            $date_created   = $user['date_created'];
            $data = [
                'head'          => 'Hak akses Menu',
                'name'          => $name,
                'img'           => $img,
                'date_created'  => $date_created
            ];

            $this->load->view('templates/head', $data);
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('menu/add', $data);
            $this->load->view('templates/footer');
        }
    }

    public function edit()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'Menu',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['menu'] = $this->db->get_where('menu', ['id' => $this->uri->segment(3)])->row_array();


        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('menu/edit',$data);
        $this->load->view('templates/footer');
    }

    public function update()
    {  
        $id = $this->uri->segment(3);
        $data = ($_POST);
        $this->menu->update($data,$id);
        $this->session->set_flashdata('message', berhasil_update);
        redirect('menu');
    } 

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->delete('menu', ['id' => $id]);
        $this->session->set_flashdata('message', berhasil_hapus);
        redirect('menu'); 
    }
}
