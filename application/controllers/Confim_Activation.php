<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Confim_Activation extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('profile_model', 'profile');
    }

    public function index()
    {

    	$otp  = $_GET['key'];

    	$user = $this->db->get_where('users', ['otp'=>$otp])->row_array();

    	if(is_array($user)){
    		$data = array('status_aktif'=>'1');
    		$id = $user['id'];
    		$this->profile->update($data,$id);
    		$this->confirm_success();

    	}else{
    		$this->confirm_fail();
    	}
    }

    public function confirm_success(){
    	$this->load->view('auth/confirm');
    }

    public function confirm_fail(){
    	$this->load->view('auth/confirm_fail');
    }

 }