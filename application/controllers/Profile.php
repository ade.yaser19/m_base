<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        session();
        $this->load->model('profile_model', 'profile');
    }

    public function index()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $tentang   = $user['tentang'];
        $twitter   = $user['twitter'];
        $instagram = $user['instagram'];
        $facebook  = $user['facebook'];
        $linkedin  = $user['linkedin'];
        $alamat    = $user['alamat'];
        $negara    = $user['negara'];
        $perusahaan = $user['perusahaan'];
        $telepon = $user['telepon'];
        $email = $user['email'];
        $job = $user['job'];

        $date_created = $user['date_created'];
        $data = [
            'head'          => 'Profile',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'twitter'       => $twitter,
            'instagram'     => $instagram,
            'facebook'      => $facebook,
            'linkedin'      => $linkedin,
            'tentang'       => $tentang,
            'alamat'        => $alamat,
            'negara'        => $negara,
            'perusahaan'    => $perusahaan,
            'telepon'       => $telepon,
            'email'         => $email,
            'job'           => $job
        ];
        

        $data['profile'] = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/head');
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('profile/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit()
    { 
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $tentang   = $_POST['tentang'];
        $twitter   = $_POST['twitter'];
        $instagram = $_POST['instagram'];
        $facebook  = $_POST['facebook'];
        $linkedin  = $_POST['linkedin'];
        $alamat    = $_POST['alamat'];
        $negara    = $_POST['negara'];
        $perusahaan = $_POST['perusahaan'];
        $telepon    = $_POST['telepon'];
        $email = $_POST['email'];
        $job = $_POST['job'];

        $date_created = $user['date_created'];
        $data = [
            'head'          => 'Profile',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['profile'] = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();


        $this->form_validation->set_rules('nama', 'Nama', 'trim|required', [
            'required' => 'Nama tidak boleh kosong'
        ]);

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email', [
            'required' => 'Email tidak boleh kosong'
        ]);

        if ($this->form_validation->run() == false) {
          

        } else {
            $data = [
                'nama' => htmlspecialchars($this->input->post('nama'), TRUE),
                'email' => htmlspecialchars($this->input->post('email'), TRUE),
                'twitter'       => $twitter,
                'instagram'     => $instagram,
                'facebook'      => $facebook,
                'linkedin'      => $linkedin,
                'tentang'       => $tentang,
                'alamat'        => $alamat,
                'negara'        => $negara,
                'perusahaan'    => $perusahaan,
                'telepon'       => $telepon,
                'email'         => $email,
                'job'           => $job
            ];

            $id = $this->session->userdata('user_id');

             $this->profile->update($data, $id);
             $this->_uploadImage();

            redirect('auth/logout');
        }
    }

    public function password()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $tentang   =$user['tentang'];
        $twitter   =$user['twitter'];
        $instagram =$user['instagram'];
        $facebook  =$user['facebook'];
        $linkedin  =$user['linkedin'];
        $alamat    =$user['alamat'];
        $negara    =$user['negara'];
        $perusahaan =$user['perusahaan'];
        $telepon    =$user['telepon'];
        $email      =$user['email'];
        $job        =$user['job'];

        $data = [
            'head'          => 'Profile',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'twitter'       => $twitter,
            'instagram'     => $instagram,
            'facebook'      => $facebook,
            'linkedin'      => $linkedin,
            'tentang'       => $tentang,
            'alamat'        => $alamat,
            'negara'        => $negara,
            'perusahaan'    => $perusahaan,
            'telepon'       => $telepon,
            'email'         => $email,
            'job'           => $job
        ];
        

        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]', [
            'required' => 'Kata sandi tidak boleh kosong',
            'min_length' => 'Kata sandi terlalu pendek'
        ]);

        $this->form_validation->set_rules('newpassword', 'Newpassword', 'trim|matches[renewpassword]', [
            'matches' => 'Kata sandi tidak sama'
        ]);

        $this->form_validation->set_rules('renewpassword', 'Renewpassword', 'trim|required', [
            'required' => 'Kata sandi tidak boleh kosong'
        ]);


        if ($this->form_validation->run() == false) {
            $this->load->view('templates/head');
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('profile/index');
            $this->load->view('templates/footer');
        } else {
            $data = [
                'password' => password_hash($this->input->post('newpassword'), PASSWORD_DEFAULT),
                'password_hint' =>$this->input->post('newpassword')
            ];

            $password = htmlspecialchars($this->input->post('password',TRUE));

            $this->profile->updatePassword($data, $password);
        }
    }

    public function image()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'head'          => 'Profile',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];
        $this->form_validation->set_rules('img', 'Img', 'trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/head');
            $this->load->view('templates/nav', $data);
            $this->load->view('templates/sidebar');
            $this->load->view('profile/editImage', $data);
            $this->load->view('templates/footer');
        } else {
            $upload = $this->_uploadImage();
           return  $upload;
        }
    }

    private function _uploadImage()
    {
        $config['upload_path'] = FCPATH . 'assets/img/profile/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '6000';
        $config['max_width']            = 6000; // 6000px you can set the value you want
        $config['max_height']           = 6000; // 6000px


        $this->load->library('upload');
        $this->upload->initialize($config);

        $img = "file_name";
        if (!$this->upload->do_upload($img)) {

            $errors = $this->upload->display_errors();
            return $errors;
    
        } else {
            $data = array('upload_data' => $this->upload->data());
            $file_name = $data['upload_data']['file_name'];

            $this->profile->updateImage($file_name);
            $messages = $this->session->set_flashdata('message', berhasil_update);
            return $messages;
        }
    }
}
