  <aside id="sidebar" class="sidebar">
    <ul class="sidebar-nav" id="sidebar-nav">
         <?php
                $style_text ="";
                $style_link ="";
                $role = $this->session->userdata('role_id');
                $QueryMenu = "   SELECT * FROM `menu` JOIN `user_access_menu` 
                            ON `menu`.`id` = `user_access_menu`.`menu_id`
                            WHERE `user_access_menu`.`role_id` = $role 
                            ORDER BY `user_access_menu`.`menu_id` ASC
                ";
                $menu = $this->db->query($QueryMenu)->result_array();
                ?>
             <?php foreach ($menu as $m) : ?>
                 <?php if ($m['url'] == $this->uri->segment(1)) : ?>
                     <?php 
                       $style_text ="style='color:blue;'";
                       $style_link ="style='background-color:#D7F6F8;'";
                     ?>
                      <li class="active" <?=$style_link?>>
                  <?php else : ?>
                    <?php 
                       $style_text ="";  
                       $style_link ="";?>
                       <li <?=$style_link?>>
                  <?php endif; ?>
                      <a class="nav-link collapsed" href="<?= base_url($m['url']); ?>">
                        <i class="<?= $m['icon']; ?>"></i>
                        <span  <?=$style_text;?>><?= $m['title']; ?></span>
                      </a>
                    </li>
             <?php endforeach; ?>
    </ul>

  </aside><!-- End Sidebar-->

     <!-- Main content -->
<section class="content">