<?= $this->session->flashdata('message'); ?>
  <main id="main" class="main">
    <div class="pagetitle">
      <h1>Detail Data Pengguna</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
          <li class="breadcrumb-item active">Detail Data Pengguna</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <div class="box">
                <h5 class="card-title">Detail Data Pengguna</h5>

            <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Nama</th>
                            <th><?= $name; ?></th>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <th><?= $email; ?></th>
                        </tr>
                        <tr>
                            <th>Kata Sandi</th>
                            <th><?= $password_hint; ?></th>
                        </tr>
                        <tr>
                            <th>Gambar</th>
                            <th> <img src="<?= base_url('assets/img/profile/') . $img; ?>" width="100px"></th>
                        </tr>
                        <tr>
                            <th>Tanggal Daftar</th>

                            <th> <?php 
                                $tanggal = $date_created;
                                echo date("'D, d M Y'",strtotime($tanggal));
                                ?></th>
                        </tr>
                    </table>
                </div>
            </div>
         </div>
        </div>
      </div>
    </section>
</main>