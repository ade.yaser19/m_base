<main id="main" class="main">
    <div class="pagetitle">
      <h1>Edit Data Pengguna</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
          <li class="breadcrumb-item active">Edit Data Pengguna</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <div class="box">
                <h5 class="card-title">Edit Data Pengguna</h5>
            
                        <!-- /.box-header -->
                    <?= validation_errors(); ?>
                    <form action="<?= base_url('users/edit/'); ?>" method="post" class="form">
                        <input type="hidden" name="id" value="<?= $id; ?>">
                        <div class="form-group mb-1 mt-1">
                            <label for="role_id">Hak Akses</label>
                            <select class="form-control js-example-basic-single" id="role_id" name="role_id" >
                                <?php
                                    $selected ="";
                                    $list_role= $this->db->get('role')->result_array();
                                    foreach($list_role as $list_role){ 
                                    $selected ="";
                                    if($list_role['id']==$role_id){
                                        $selected ="selected";
                                    }
                                ?>
                            <option value="<?php echo $list_role['id'];?>" <?=$selected;?>><?php echo $list_role['role'];?></option>
                                <?php }?>
                            </select>
                          <!-- input type="text" class="form-control" id="role_id" name="role_id" value="<?= $role_id; ?>" required>  -->
                        </div>
                        <div class="form-group mb-1 mt-1">
                            <label for="nama">Nama</label>
                            <input type="text" id="nama" class="form-control" name="name" value="<?= $nama; ?>" required>
                        </div>
                        <div class="form-group mb-2 mt-2">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?= $email; ?>" required>
                        </div>
                        <button class="btn btn-sm btn-primary" type="submit">Ubah</button>
                    </form>
                </div>
            </div>
         </div>
        </div>
      </div>
    </section>
</main>