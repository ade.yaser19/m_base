
<main id="main" class="main">
    <div class="pagetitle">
      <h1>Edit Data Pengguna</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
          <li class="breadcrumb-item active">Tambah Data Pengguna</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

<section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <div class="box">
                <h5 class="card-title">Tambah Data Pengguna</h5>
                <div class="box-body">
                    <form action="<?= base_url('users/add'); ?>" class="form" method="post">
                        <div class="form-group mb-1 mt-1">
                            <input type="text" name="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group mb-1 mt-1">
                            <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap">
                        </div>
                        <div class="form-group mb-1 mt-1">
                            <input type="text" name="password1" class="form-control" placeholder="Kata Sandi">
                        </div>
                        <div class="form-group mb-1 mt-1">
                            <input type="text" name="password2" class="form-control" placeholder="Ulangi Kata Sandi">
                        </div>
                        <button class="btn btn-sm btn-primary mb-1 mt-1" type="submit">Simpan</button>
                        <button class="btn btn-sm btn-warning mb-1 mt-1" type="reset">Reset</button>
                    </form>
                </div>
            </div>
    </section>
</main>