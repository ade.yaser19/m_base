  <main id="main" class="main">
<?= $this->session->flashdata('message'); ?>

    <div class="pagetitle">
      <h1>Data Pengguna</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
          <li class="breadcrumb-item active">Data Pengguna</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <div class="box">
                <div class="box-body mt-4 mb-0">
                    <a href="<?= base_url('users/add'); ?>" class="btn btn-flat btn-primary"><i class="bx bx-user-plus"></i> Tambah</a>
                </div>
              <h5 class="card-title">Data Pengguna</h5>

              <!-- Table with stripped rows -->
              <div class="table-responsive">
               <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Gambar</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
               <?php
                $x = 1;
                foreach ($user as $usr) : ?>
                    <tr>
                        <td><?= $x++; ?></td>
                        <td><?= $usr['nama']; ?></td>
                        <td><?= $usr['email']; ?></td>
                        <td><img src="<?= base_url('assets/img/profile/') . $usr['img']; ?>" class="user-image" width="40" alt="Profile"></td>
                        <td>
                            <a href="<?= base_url('users/detail/') . $usr['id']; ?>" class="btn btn-flat btn-xs  btn-info"><i class="bx bxs-detail"></i></a>
                            <a href="<?= base_url('users/edit/') . $usr['id']; ?>" class="btn btn-flat btn-xs  btn-warning"><i class="bx bxs-message-alt-edit"></i></a>
                            <a onclick="return confirm('Hapus ?' );" href="<?= base_url('users/delete/') . $usr['id']; ?>" class="btn btn-flat btn-xs  btn-danger"><i class="bx bxs-eraser"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
              </div>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>
</main>