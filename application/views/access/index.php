<main id="main" class="main"> <?= $this->session->flashdata('message'); ?> <div class="pagetitle">
    <h1>Hak Aksess</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="
            <?=base_url()?>">Home </a>
        </li>
        <li class="breadcrumb-item active">Data Hak Akses Menu</li>
      </ol>
    </nav>
  </div>
  <section class="section">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="box">
              <div class="box-body mt-4 mb-0">
                <a href="<?= base_url('access/add'); ?>" class="btn btn-flat btn-primary">
                  <i class="bx bx-user-plus"></i> Tambah </a>
              </div>
              <h5 class="card-title">Data Hak Aksess Menu</h5>
              <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Hak Akses</th>
                      <th>Nama Menu</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody> <?php
                        $x = 1;
                        foreach ($access as $acc) : ?> <tr>
                      <td> <?= $x++; ?> </td>
                      <td> <?= $acc['role']; ?> </td>
                      <td> <?= $acc['title']; ?> </td>
                      <td>
                        <a onclick="return confirm('Hapus ?' );" href="
                          <?= base_url('access/delete/') . $acc['id']; ?>" class="btn btn-flat btn-xs  btn-danger">
                          <i class="bx bx-trash"></i>
                        </a>
                      </td>
                    </tr> <?php endforeach; ?> </tbody>
                </table>
                <!-- End Table with stripped rows -->
              </div>
            </div>
          </div>
        </div>
  </section>
</main>