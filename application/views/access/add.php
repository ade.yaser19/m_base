<main id="main" class="main">
<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

<?= $this->session->flashdata('message'); ?>

    <div class="pagetitle">
      <h3 class="box-title">Role Menu</h3>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
          <li class="breadcrumb-item active">Role Menu</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <div class="box">
                <div class="box-body mt-4 mb-0">
                </div>

                <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Tambah Hak Akses Menu</h3>
                </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="<?= base_url('access/add'); ?>" method="post" class="form">
                    <div class="form-group mt-2 mb-2">
                        <label for="role_id">ID Role atau ID Hak Akses</label>
                        <select class="form-control" name="role_id" id ="role_id"> 
                            <?php
                                $list_role= $this->db->get('role')->result_array();
                                foreach($list_role as $list_role){ 
                            ?>
                              <option value="<?=$list_role['id']?>"><?=$list_role['role']?></option>
                            <?php }?>
                     </select>
                    </div>
                    <div class="form-group mt-2 mb-2">
                        <label for="menu_id">ID Menu</label>
                         <select class="form-control" name="menu_id" id ="menu_id"> 
                            <?php
                                $menu_id= $this->db->get('menu')->result_array();
                                foreach($menu_id as $menu_id){ 
                            ?>
                              <option value="<?=$menu_id['id']?>"><?=$menu_id['title']?></option>
                            <?php }?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary mt-2 mb-2">Simpan</button>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
</main>