
<main id="main" class="main"> 
<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

    <h1>Hak Aksess</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="
            <?=base_url()?>">Home </a>
        </li>
        <li class="breadcrumb-item active">Data Hak Akses</li>
      </ol>
    </nav>
  </div>
  <section class="section">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
              <h5 class="card-title">Data Hak Aksess</h5>
                <form action="<?= base_url('role/add'); ?>" method="post" class="form">
                    <div class="form-group">
                        <label for="role">Nama Hak Akses</label>
                        <input type="text" name="role" id="role" class="form-control" value="<?= set_value('role'); ?>">
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary mt-2">Simpan</button>
                </form>
              </div>
            </div>
          </div>
        </div>
  </section>
</main>