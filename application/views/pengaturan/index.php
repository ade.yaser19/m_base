<?= $this->session->flashdata('message'); ?>
<main id="main" class="main">
    <div class="pagetitle">
      <h1>Profile</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
          <li class="breadcrumb-item active">Pengaturan</li>
        </ol>
      </nav>
    </div>
    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <div class="box">
              <h5 class="card-title">Informasi Sistem</h5>
              <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Nama Sistem</th>
                        <td><?= $pengaturan['nama_sistem']; ?></td>
                        <td><a href="<?= base_url('pengaturan/edit'); ?>" class="btn btn-sm btn-primary"> <i class="fa fa-pencil"></i> Edit</a></td>
                    </tr>
                </table>
              </div>
              </div>
            </div>
         </div>
      </div>
    </section>
</main>