
<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>
<main id="main" class="main">
    <div class="pagetitle">
      <h1>Profile</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
          <li class="breadcrumb-item active">Pengaturan</li>
        </ol>
      </nav>
    </div>
    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <div class="box">
              <h5 class="card-title">Informasi Sistem</h5>
                <form action="<?= base_url('pengaturan/edit') ?>" class="form" method="post">
                    <div class="form-group">
                        <label for="nama_sistem">Nama sistem</label>
                        <input type="text" class="form-control" name="nama_sistem" id="nama_sistem" value="<?= $pengaturan['nama_sistem']; ?>">
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary mt-2">Simpan</button>
                </form>
              </div>
            </div>
         </div>
      </div>
    </section>
</main>