<?= validation_errors(
	'<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
	'</div>'
); ?>

<?= $this->session->flashdata('message'); ?>
  <main id="main" class="main">

    <div class="pagetitle">
      <h1>Data Menu</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
          <li class="breadcrumb-item active">Edit Menu</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <div class="box">
      
              <h5 class="card-title">Edit Menu</h5>

			    <form action="<?= base_url('menu/update/'.$menu['id']); ?>" class="form" method="post">
					
					<div class="form-group">
						<label>Nama Menu</label>
						<input type="text" class="form-control" name="title" value="<?= $menu['title']; ?>">
					</div>

					<div class="form-group mt-1 mb-1">
					<label>Icon Menu</label>
						<select class="form-control" name="icon" id ="icon">
		                    <?php
		                    $selected ="";
		                    $list_icon= $this->db->get('icon')->result_array();
		                    	foreach($list_icon as $list_icon){
		                    	$selected='';
		                    	if($list_icon['inisial'].' '.$list_icon['icon']==$menu['icon']){
		                    		$selected ="selected";
		                    	}

		                    	?>
		                    <option value="<?=$list_icon['inisial']?> <?=$list_icon['icon']?>" <?=$selected?>><?=$list_icon['unicode']?> <?=$list_icon['icon']?></option> 
		                    <?php }?>
		                </select>
					</div>

					<div class="form-group mt-1 mb-1">
						<label>Url Menu</label>
						<input type="text" class="form-control" name="url" value="<?= $menu['url']; ?>">
					</div>

					<div class="form-group mt-1 mb-1">
						<label>Status Menu</label>
						<select class="form-control" name="is_active" id ="is_active">
							<?php 
							 $selected1 ="";
							if($menu['is_active'] ==1){
								$selected1 ="selected";
								echo '<option value="1" '.$selected1.'>Aktif</option>';
								echo '<option value="2">Non Aktif</option> ';
							}else{
								$selected1 ="selected";
								echo '<option value="2" '.$selected1.'>Non Aktif</option> ';
								echo '<option value="1">Aktif</option>';
							}

							?>

						</select>
					</div>

					<button type="submit" class="btn btn-primary mt-1 mb-1">Save</button>
					<button type="reset" class="btn btn-warning mt-1 mb-1">Reset</button>
				</form>
            </div>
          </div>
        </div>
      </div>
  </section>
</main>