<?= validation_errors(
	'<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
	'</div>'
); ?>

<?= $this->session->flashdata('message'); ?>
  <main id="main" class="main">

    <div class="pagetitle">
      <h1>Data Menu</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
          <li class="breadcrumb-item active">Add Menu</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <div class="box">
      
              <h5 class="card-title">Add Menu</h5>

				<form action="<?= base_url('menu/add'); ?>" class="form" method="POST">
					
					<div class="form-group mt-1 mb-1">
						<label>Nama Menu</label>
						<input type="text" class="form-control" name="title" value="">
					</div>

					<div class="form-group mt-1 mb-1">
						<label>Icon Menu</label>
						<select class="form-control" name="icon" id ="icon"> 
                                <?php
                                    $list_icon= $this->db->get('icon')->result_array();
                                    foreach($list_icon as $list_icon){ 
                                ?>
                            	  <option value="<?=$list_icon['inisial']?> <?=$list_icon['icon']?>"><?=$list_icon['unicode']?> <?=$list_icon['icon']?></option>
                                <?php }?>
             </select>
					</div>
					
					<div class="form-group mt-1 mb-1">
						<label>Url Menu</label>
						<input type="text" class="form-control" name="url" value="">
					</div>
					
					<a href="<?= base_url('menu') ?>" class="btn btn-sm btn-warning mt-1 mb-1" style ="float: right; margin-left: 5px;">Kembali</a>
					<button type="submit" class="btn btn-sm btn-primary mt-1 mb-1" style ="float: right;">Simpan</button>
				</form>
            </div>
          </div>
        </div>
      </div>
    </section>
</main>