<main id="main" class="main">
<?= $this->session->flashdata('message'); ?>
    <div class="pagetitle">
      <h1>Data Menu</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
          <li class="breadcrumb-item active">Data Menu</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <div class="box">
                    <div class="box-body mt-4 mb-0">
                        <a href="<?= base_url('menu/add'); ?>" class="btn btn-flat btn-primary"><i class="bx bxs-bookmark-alt-plus"></i> Tambah</a>
                    </div>
              <h5 class="card-title">Data Menu</h5>

                      <!-- Table with stripped rows -->
                <div class="table-responsive">
                      <table class="table datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Menu</th>
                            <th>Icon</th>
                            <th>Link</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $x = 1;
                        foreach ($menu as $m) : ?>
                            <tr>
                                <td><?= $x++; ?></td>
                                <td><?= $m['title']; ?></td>
                                <td><?= $m['icon']; ?>-<i class="<?= $m['icon']; ?>"></i></td>
                                <td><?= $m['url']; ?></td>
                                <td><?= $m['is_active']==1 ? 'Aktif':'Tidak Aktif' ?></td>
                                <td>
                                    <a href="<?=base_url('menu/edit/').$m['id']?>" class="btn btn-flat btn-xs  btn-warning"><i class="bx bxs-pencil"></i></a>
                                    <a href="<?=base_url('menu/delete/').$m['id']?>" class="btn btn-flat btn-xs  btn-danger"><i class="bx bxs-eraser"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>
</main>