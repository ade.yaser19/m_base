-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2024 at 03:13 PM
-- Server version: 10.6.16-MariaDB-cll-lve
-- PHP Version: 8.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ade40622_m_base`
--

-- --------------------------------------------------------

--
-- Table structure for table `icon`
--

CREATE TABLE `icon` (
  `id` int(11) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `inisial` varchar(100) NOT NULL,
  `unicode` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `icon`
--

INSERT INTO `icon` (`id`, `icon`, `inisial`, `unicode`) VALUES
(1, 'fa-address-book', 'fa', '&#xf2b9;'),
(2, 'fa-address-book-o', 'fa', '&#xf2ba;'),
(3, 'fa-address-card', 'fa', '&#xf2bb;'),
(4, 'fa-address-card-o', 'fa', '&#xf2bc;'),
(5, 'fa-adjust', 'fa', '&#xf042;'),
(6, 'fa-adn', 'fa', '&#xf170;'),
(7, 'fa-align-center', 'fa', '&#xf037;'),
(8, 'fa-align-justify', 'fa', '&#xf039;'),
(9, 'fa-align-left', 'fa', '&#xf036;'),
(10, 'fa-align-right', 'fa', '&#xf038;'),
(11, 'fa-amazon', 'fa', '&#xf270;'),
(12, 'fa-ambulance', 'fa', '&#xf0f9;'),
(13, 'fa-american-sign-language-interpreting', 'fa', '&#xf2a3;'),
(14, 'fa-anchor', 'fa', '&#xf13d;'),
(15, 'fa-android', 'fa', '&#xf17b;'),
(16, 'fa-angellist', 'fa', '&#xf209;'),
(17, 'fa-angle-double-down', 'fa', '&#xf103;'),
(18, 'fa-angle-double-left', 'fa', '&#xf100;'),
(19, 'fa-angle-double-right', 'fa', '&#xf101;'),
(20, 'fa-angle-double-up', 'fa', '&#xf102;'),
(21, 'fa-angle-down', 'fa', '&#xf107;'),
(22, 'fa-angle-left', 'fa', '&#xf104;'),
(23, 'fa-angle-right', 'fa', '&#xf105;'),
(24, 'fa-angle-up', 'fa', '&#xf106;'),
(25, 'fa-apple', 'fa', '&#xf179;'),
(26, 'fa-archive', 'fa', '&#xf187;'),
(27, 'fa-area-chart', 'fa', '&#xf1fe;'),
(28, 'fa-arrow-circle-down', 'fa', '&#xf0ab;'),
(29, 'fa-arrow-circle-left', 'fa', '&#xf0a8;'),
(30, 'fa-arrow-circle-o-down', 'fa', '&#xf01a;'),
(31, 'fa-arrow-circle-o-left', 'fa', '&#xf190;'),
(32, 'fa-arrow-circle-o-right', 'fa', '&#xf18e;'),
(33, 'fa-arrow-circle-o-up', 'fa', '&#xf01b;'),
(34, 'fa-arrow-circle-right', 'fa', '&#xf0a9;'),
(35, 'fa-arrow-circle-up', 'fa', '&#xf0aa;'),
(36, 'fa-arrow-down', 'fa', '&#xf063;'),
(37, 'fa-arrow-left', 'fa', '&#xf060;'),
(38, 'fa-arrow-right', 'fa', '&#xf061;'),
(39, 'fa-arrow-up', 'fa', '&#xf062;'),
(40, 'fa-arrows', 'fa', '&#xf047;'),
(41, 'fa-arrows-alt', 'fa', '&#xf0b2;'),
(42, 'fa-arrows-h', 'fa', '&#xf07e;'),
(43, 'fa-arrows-v', 'fa', '&#xf07d;'),
(44, 'fa-asl-interpreting', 'fa', '&#xf2a3;'),
(45, 'fa-assistive-listening-systems', 'fa', '&#xf2a2;'),
(46, 'fa-asterisk', 'fa', '&#xf069;'),
(47, 'fa-at', 'fa', '&#xf1fa;'),
(48, 'fa-audio-description', 'fa', '&#xf29e;'),
(49, 'fa-automobile', 'fa', '&#xf1b9;'),
(50, 'fa-backward', 'fa', '&#xf04a;'),
(51, 'fa-balance-scale', 'fa', '&#xf24e;'),
(52, 'fa-ban', 'fa', '&#xf05e;'),
(53, 'fa-bandcamp', 'fa', '&#xf2d5;'),
(54, 'fa-bank', 'fa', '&#xf19c;'),
(55, 'fa-bar-chart', 'fa', '&#xf080;'),
(56, 'fa-bar-chart-o', 'fa', '&#xf080;'),
(57, 'fa-barcode', 'fa', '&#xf02a;'),
(58, 'fa-bars', 'fa', '&#xf0c9;'),
(59, 'fa-bath', 'fa', '&#xf2cd;'),
(60, 'fa-bathtub', 'fa', '&#xf2cd;'),
(61, 'fa-battery', 'fa', '&#xf240;'),
(62, 'fa-battery-0', 'fa', '&#xf244;'),
(63, 'fa-battery-1', 'fa', '&#xf243;'),
(64, 'fa-battery-2', 'fa', '&#xf242;'),
(65, 'fa-battery-3', 'fa', '&#xf241;'),
(66, 'fa-battery-4', 'fa', '&#xf240;'),
(67, 'fa-battery-empty', 'fa', '&#xf244;'),
(68, 'fa-battery-full', 'fa', '&#xf240;'),
(69, 'fa-battery-half', 'fa', '&#xf242;'),
(70, 'fa-battery-quarter', 'fa', '&#xf243;'),
(71, 'fa-battery-three-quarters', 'fa', '&#xf241;'),
(72, 'fa-bed', 'fa', '&#xf236;'),
(73, 'fa-beer', 'fa', '&#xf0fc;'),
(74, 'fa-behance', 'fa', '&#xf1b4;'),
(75, 'fa-behance-square', 'fa', '&#xf1b5;'),
(76, 'fa-bell', 'fa', '&#xf0f3;'),
(77, 'fa-bell-o', 'fa', '&#xf0a2;'),
(78, 'fa-bell-slash', 'fa', '&#xf1f6;'),
(79, 'fa-bell-slash-o', 'fa', '&#xf1f7;'),
(80, 'fa-bicycle', 'fa', '&#xf206;'),
(81, 'fa-binoculars', 'fa', '&#xf1e5;'),
(82, 'fa-birthday-cake', 'fa', '&#xf1fd;'),
(83, 'fa-bitbucket', 'fa', '&#xf171;'),
(84, 'fa-bitbucket-square', 'fa', '&#xf172;'),
(85, 'fa-bitcoin', 'fa', '&#xf15a;'),
(86, 'fa-black-tie', 'fa', '&#xf27e;'),
(87, 'fa-blind', 'fa', '&#xf29d;'),
(88, 'fa-bluetooth', 'fa', '&#xf293;'),
(89, 'fa-bluetooth-b', 'fa', '&#xf294;'),
(90, 'fa-bold', 'fa', '&#xf032;'),
(91, 'fa-bolt', 'fa', '&#xf0e7;'),
(92, 'fa-bomb', 'fa', '&#xf1e2;'),
(93, 'fa-book', 'fa', '&#xf02d;'),
(94, 'fa-bookmark', 'fa', '&#xf02e;'),
(95, 'fa-bookmark-o', 'fa', '&#xf097;'),
(96, 'fa-braille', 'fa', '&#xf2a1;'),
(97, 'fa-briefcase', 'fa', '&#xf0b1;'),
(98, 'fa-btc', 'fa', '&#xf15a;'),
(99, 'fa-bug', 'fa', '&#xf188;'),
(100, 'fa-building', 'fa', '&#xf1ad;'),
(101, 'fa-building-o', 'fa', '&#xf0f7;'),
(102, 'fa-bullhorn', 'fa', '&#xf0a1;'),
(103, 'fa-bullseye', 'fa', '&#xf140;'),
(104, 'fa-bus', 'fa', '&#xf207;'),
(105, 'fa-buysellads', 'fa', '&#xf20d;'),
(106, 'fa-cab', 'fa', '&#xf1ba;'),
(107, 'fa-calculator', 'fa', '&#xf1ec;'),
(108, 'fa-calendar', 'fa', '&#xf073;'),
(109, 'fa-calendar-check-o', 'fa', '&#xf274;'),
(110, 'fa-calendar-minus-o', 'fa', '&#xf272;'),
(111, 'fa-calendar-o', 'fa', '&#xf133;'),
(112, 'fa-calendar-plus-o', 'fa', '&#xf271;'),
(113, 'fa-calendar-times-o', 'fa', '&#xf273;'),
(114, 'fa-camera', 'fa', '&#xf030;'),
(115, 'fa-camera-retro', 'fa', '&#xf083;'),
(116, 'fa-car', 'fa', '&#xf1b9;'),
(117, 'fa-caret-down', 'fa', '&#xf0d7;'),
(118, 'fa-caret-left', 'fa', '&#xf0d9;'),
(119, 'fa-caret-right', 'fa', '&#xf0da;'),
(120, 'fa-caret-square-o-down', 'fa', '&#xf150;'),
(121, 'fa-caret-square-o-left', 'fa', '&#xf191;'),
(122, 'fa-caret-square-o-right', 'fa', '&#xf152;'),
(123, 'fa-caret-square-o-up', 'fa', '&#xf151;'),
(124, 'fa-caret-up', 'fa', '&#xf0d8;'),
(125, 'fa-cart-arrow-down', 'fa', '&#xf218;'),
(126, 'fa-cart-plus', 'fa', '&#xf217;'),
(127, 'fa-cc', 'fa', '&#xf20a;'),
(128, 'fa-cc-amex', 'fa', '&#xf1f3;'),
(129, 'fa-cc-diners-club', 'fa', '&#xf24c;'),
(130, 'fa-cc-discover', 'fa', '&#xf1f2;'),
(131, 'fa-cc-jcb', 'fa', '&#xf24b;'),
(132, 'fa-cc-mastercard', 'fa', '&#xf1f1;'),
(133, 'fa-cc-paypal', 'fa', '&#xf1f4;'),
(134, 'fa-cc-stripe', 'fa', '&#xf1f5;'),
(135, 'fa-cc-visa', 'fa', '&#xf1f0;'),
(136, 'fa-certificate', 'fa', '&#xf0a3;'),
(137, 'fa-chain', 'fa', '&#xf0c1;'),
(138, 'fa-chain-broken', 'fa', '&#xf127;'),
(139, 'fa-check', 'fa', '&#xf00c;'),
(140, 'fa-check-circle', 'fa', '&#xf058;'),
(141, 'fa-check-circle-o', 'fa', '&#xf05d;'),
(142, 'fa-check-square', 'fa', '&#xf14a;'),
(143, 'fa-check-square-o', 'fa', '&#xf046;'),
(144, 'fa-chevron-circle-down', 'fa', '&#xf13a;'),
(145, 'fa-chevron-circle-left', 'fa', '&#xf137;'),
(146, 'fa-chevron-circle-right', 'fa', '&#xf138;'),
(147, 'fa-chevron-circle-up', 'fa', '&#xf139;'),
(148, 'fa-chevron-down', 'fa', '&#xf078;'),
(149, 'fa-chevron-left', 'fa', '&#xf053;'),
(150, 'fa-chevron-right', 'fa', '&#xf054;'),
(151, 'fa-chevron-up', 'fa', '&#xf077;'),
(152, 'fa-child', 'fa', '&#xf1ae;'),
(153, 'fa-chrome', 'fa', '&#xf268;'),
(154, 'fa-circle', 'fa', '&#xf111;'),
(155, 'fa-circle-o', 'fa', '&#xf10c;'),
(156, 'fa-circle-o-notch', 'fa', '&#xf1ce;'),
(157, 'fa-circle-thin', 'fa', '&#xf1db;'),
(158, 'fa-clipboard', 'fa', '&#xf0ea;'),
(159, 'fa-clock-o', 'fa', '&#xf017;'),
(160, 'fa-clone', 'fa', '&#xf24d;'),
(161, 'fa-close', 'fa', '&#xf00d;'),
(162, 'fa-cloud', 'fa', '&#xf0c2;'),
(163, 'fa-cloud-download', 'fa', '&#xf0ed;'),
(164, 'fa-cloud-upload', 'fa', '&#xf0ee;'),
(165, 'fa-cny', 'fa', '&#xf157;'),
(166, 'fa-code', 'fa', '&#xf121;'),
(167, 'fa-code-fork', 'fa', '&#xf126;'),
(168, 'fa-codepen', 'fa', '&#xf1cb;'),
(169, 'fa-codiepie', 'fa', '&#xf284;'),
(170, 'fa-coffee', 'fa', '&#xf0f4;'),
(171, 'fa-cog', 'fa', '&#xf013;'),
(172, 'fa-cogs', 'fa', '&#xf085;'),
(173, 'fa-columns', 'fa', '&#xf0db;'),
(174, 'fa-comment', 'fa', '&#xf075;'),
(175, 'fa-comment-o', 'fa', '&#xf0e5;'),
(176, 'fa-commenting', 'fa', '&#xf27a;'),
(177, 'fa-commenting-o', 'fa', '&#xf27b;'),
(178, 'fa-comments', 'fa', '&#xf086;'),
(179, 'fa-comments-o', 'fa', '&#xf0e6;'),
(180, 'fa-compass', 'fa', '&#xf14e;'),
(181, 'fa-compress', 'fa', '&#xf066;'),
(182, 'fa-connectdevelop', 'fa', '&#xf20e;'),
(183, 'fa-contao', 'fa', '&#xf26d;'),
(184, 'fa-copy', 'fa', '&#xf0c5;'),
(185, 'fa-copyright', 'fa', '&#xf1f9;'),
(186, 'fa-creative-commons', 'fa', '&#xf25e;'),
(187, 'fa-credit-card', 'fa', '&#xf09d;'),
(188, 'fa-credit-card-alt', 'fa', '&#xf283;'),
(189, 'fa-crop', 'fa', '&#xf125;'),
(190, 'fa-crosshairs', 'fa', '&#xf05b;'),
(191, 'fa-css3', 'fa', '&#xf13c;'),
(192, 'fa-cube', 'fa', '&#xf1b2;'),
(193, 'fa-cubes', 'fa', '&#xf1b3;'),
(194, 'fa-cut', 'fa', '&#xf0c4;'),
(195, 'fa-cutlery', 'fa', '&#xf0f5;'),
(196, 'fa-dashboard', 'fa', '&#xf0e4;'),
(197, 'fa-dashcube', 'fa', '&#xf210;'),
(198, 'fa-database', 'fa', '&#xf1c0;'),
(199, 'fa-deaf', 'fa', '&#xf2a4;'),
(200, 'fa-deafness', 'fa', '&#xf2a4;'),
(201, 'fa-dedent', 'fa', '&#xf03b;'),
(202, 'fa-delicious', 'fa', '&#xf1a5;'),
(203, 'fa-desktop', 'fa', '&#xf108;'),
(204, 'fa-deviantart', 'fa', '&#xf1bd;'),
(205, 'fa-diamond', 'fa', '&#xf219;'),
(206, 'fa-digg', 'fa', '&#xf1a6;'),
(207, 'fa-dollar', 'fa', '&#xf155;'),
(208, 'fa-dot-circle-o', 'fa', '&#xf192;'),
(209, 'fa-download', 'fa', '&#xf019;'),
(210, 'fa-dribbble', 'fa', '&#xf17d;'),
(211, 'fa-drivers-license', 'fa', '&#xf2c2;'),
(212, 'fa-drivers-license-o', 'fa', '&#xf2c3;'),
(213, 'fa-dropbox', 'fa', '&#xf16b;'),
(214, 'fa-drupal', 'fa', '&#xf1a9;'),
(215, 'fa-edge', 'fa', '&#xf282;'),
(216, 'fa-edit', 'fa', '&#xf044;'),
(217, 'fa-eercast', 'fa', '&#xf2da;'),
(218, 'fa-eject', 'fa', '&#xf052;'),
(219, 'fa-ellipsis-h', 'fa', '&#xf141;'),
(220, 'fa-ellipsis-v', 'fa', '&#xf142;'),
(221, 'fa-empire', 'fa', '&#xf1d1;'),
(222, 'fa-envelope', 'fa', '&#xf0e0;'),
(223, 'fa-envelope-o', 'fa', '&#xf003;'),
(224, 'fa-envelope-open', 'fa', '&#xf2b6;'),
(225, 'fa-envelope-open-o', 'fa', '&#xf2b7;'),
(226, 'fa-envelope-square', 'fa', '&#xf199;'),
(227, 'fa-envira', 'fa', '&#xf299;'),
(228, 'fa-eraser', 'fa', '&#xf12d;'),
(229, 'fa-etsy', 'fa', '&#xf2d7;'),
(230, 'fa-eur', 'fa', '&#xf153;'),
(231, 'fa-euro', 'fa', '&#xf153;'),
(232, 'fa-exchange', 'fa', '&#xf0ec;'),
(233, 'fa-exclamation', 'fa', '&#xf12a;'),
(234, 'fa-exclamation-circle', 'fa', '&#xf06a;'),
(235, 'fa-exclamation-triangle', 'fa', '&#xf071;'),
(236, 'fa-expand', 'fa', '&#xf065;'),
(237, 'fa-expeditedssl', 'fa', '&#xf23e;'),
(238, 'fa-external-link', 'fa', '&#xf08e;'),
(239, 'fa-external-link-square', 'fa', '&#xf14c;'),
(240, 'fa-eye', 'fa', '&#xf06e;'),
(241, 'fa-eye-slash', 'fa', '&#xf070;'),
(242, 'fa-eyedropper', 'fa', '&#xf1fb;'),
(243, 'fa-fa', 'fa', '&#xf2b4;'),
(244, 'fa-facebook', 'fa', '&#xf09a;'),
(245, 'fa-facebook-f', 'fa', '&#xf09a;'),
(246, 'fa-facebook-official', 'fa', '&#xf230;'),
(247, 'fa-facebook-square', 'fa', '&#xf082;'),
(248, 'fa-fast-backward', 'fa', '&#xf049;'),
(249, 'fa-fast-forward', 'fa', '&#xf050;'),
(250, 'fa-fax', 'fa', '&#xf1ac;'),
(251, 'fa-feed', 'fa', '&#xf09e;'),
(252, 'fa-female', 'fa', '&#xf182;'),
(253, 'fa-fighter-jet', 'fa', '&#xf0fb;'),
(254, 'fa-file', 'fa', '&#xf15b;'),
(255, 'fa-file-archive-o', 'fa', '&#xf1c6;'),
(256, 'fa-file-audio-o', 'fa', '&#xf1c7;'),
(257, 'fa-file-code-o', 'fa', '&#xf1c9;'),
(258, 'fa-file-excel-o', 'fa', '&#xf1c3;'),
(259, 'fa-file-image-o', 'fa', '&#xf1c5;'),
(260, 'fa-file-movie-o', 'fa', '&#xf1c8;'),
(261, 'fa-file-o', 'fa', '&#xf016;'),
(262, 'fa-file-pdf-o', 'fa', '&#xf1c1;'),
(263, 'fa-file-photo-o', 'fa', '&#xf1c5;'),
(264, 'fa-file-picture-o', 'fa', '&#xf1c5;'),
(265, 'fa-file-powerpoint-o', 'fa', '&#xf1c4;'),
(266, 'fa-file-sound-o', 'fa', '&#xf1c7;'),
(267, 'fa-file-text', 'fa', '&#xf15c;'),
(268, 'fa-file-text-o', 'fa', '&#xf0f6;'),
(269, 'fa-file-video-o', 'fa', '&#xf1c8;'),
(270, 'fa-file-word-o', 'fa', '&#xf1c2;'),
(271, 'fa-file-zip-o', 'fa', '&#xf1c6;'),
(272, 'fa-files-o', 'fa', '&#xf0c5;'),
(273, 'fa-film', 'fa', '&#xf008;'),
(274, 'fa-filter', 'fa', '&#xf0b0;'),
(275, 'fa-fire', 'fa', '&#xf06d;'),
(276, 'fa-fire-extinguisher', 'fa', '&#xf134;'),
(277, 'fa-firefox', 'fa', '&#xf269;'),
(278, 'fa-first-order', 'fa', '&#xf2b0;'),
(279, 'fa-flag', 'fa', '&#xf024;'),
(280, 'fa-flag-checkered', 'fa', '&#xf11e;'),
(281, 'fa-flag-o', 'fa', '&#xf11d;'),
(282, 'fa-flash', 'fa', '&#xf0e7;'),
(283, 'fa-flask', 'fa', '&#xf0c3;'),
(284, 'fa-flickr', 'fa', '&#xf16e;'),
(285, 'fa-floppy-o', 'fa', '&#xf0c7;'),
(286, 'fa-folder', 'fa', '&#xf07b;'),
(287, 'fa-folder-o', 'fa', '&#xf114;'),
(288, 'fa-folder-open', 'fa', '&#xf07c;'),
(289, 'fa-folder-open-o', 'fa', '&#xf115;'),
(290, 'fa-font', 'fa', '&#xf031;'),
(291, 'fa-font-awesome', 'fa', '&#xf2b4;'),
(292, 'fa-fonticons', 'fa', '&#xf280;'),
(293, 'fa-fort-awesome', 'fa', '&#xf286;'),
(294, 'fa-forumbee', 'fa', '&#xf211;'),
(295, 'fa-forward', 'fa', '&#xf04e;'),
(296, 'fa-foursquare', 'fa', '&#xf180;'),
(297, 'fa-free-code-camp', 'fa', '&#xf2c5;'),
(298, 'fa-frown-o', 'fa', '&#xf119;'),
(299, 'fa-futbol-o', 'fa', '&#xf1e3;'),
(300, 'fa-gamepad', 'fa', '&#xf11b;'),
(301, 'fa-gavel', 'fa', '&#xf0e3;'),
(302, 'fa-gbp', 'fa', '&#xf154;'),
(303, 'fa-ge', 'fa', '&#xf1d1;'),
(304, 'fa-gear', 'fa', '&#xf013;'),
(305, 'fa-gears', 'fa', '&#xf085;'),
(306, 'fa-genderless', 'fa', '&#xf22d;'),
(307, 'fa-get-pocket', 'fa', '&#xf265;'),
(308, 'fa-gg', 'fa', '&#xf260;'),
(309, 'fa-gg-circle', 'fa', '&#xf261;'),
(310, 'fa-gift', 'fa', '&#xf06b;'),
(311, 'fa-git', 'fa', '&#xf1d3;'),
(312, 'fa-git-square', 'fa', '&#xf1d2;'),
(313, 'fa-github', 'fa', '&#xf09b;'),
(314, 'fa-github-alt', 'fa', '&#xf113;'),
(315, 'fa-github-square', 'fa', '&#xf092;'),
(316, 'fa-gitlab', 'fa', '&#xf296;'),
(317, 'fa-gittip', 'fa', '&#xf184;'),
(318, 'fa-glass', 'fa', '&#xf000;'),
(319, 'fa-glide', 'fa', '&#xf2a5;'),
(320, 'fa-glide-g', 'fa', '&#xf2a6;'),
(321, 'fa-globe', 'fa', '&#xf0ac;'),
(322, 'fa-google', 'fa', '&#xf1a0;'),
(323, 'fa-google-plus', 'fa', '&#xf0d5;'),
(324, 'fa-google-plus-circle', 'fa', '&#xf2b3;'),
(325, 'fa-google-plus-official', 'fa', '&#xf2b3;'),
(326, 'fa-google-plus-square', 'fa', '&#xf0d4;'),
(327, 'fa-google-wallet', 'fa', '&#xf1ee;'),
(328, 'fa-graduation-cap', 'fa', '&#xf19d;'),
(329, 'fa-gratipay', 'fa', '&#xf184;'),
(330, 'fa-grav', 'fa', '&#xf2d6;'),
(331, 'fa-group', 'fa', '&#xf0c0;'),
(332, 'fa-h-square', 'fa', '&#xf0fd;'),
(333, 'fa-hacker-news', 'fa', '&#xf1d4;'),
(334, 'fa-hand-grab-o', 'fa', '&#xf255;'),
(335, 'fa-hand-lizard-o', 'fa', '&#xf258;'),
(336, 'fa-hand-o-down', 'fa', '&#xf0a7;'),
(337, 'fa-hand-o-left', 'fa', '&#xf0a5;'),
(338, 'fa-hand-o-right', 'fa', '&#xf0a4;'),
(339, 'fa-hand-o-up', 'fa', '&#xf0a6;'),
(340, 'fa-hand-paper-o', 'fa', '&#xf256;'),
(341, 'fa-hand-peace-o', 'fa', '&#xf25b;'),
(342, 'fa-hand-pointer-o', 'fa', '&#xf25a;'),
(343, 'fa-hand-rock-o', 'fa', '&#xf255;'),
(344, 'fa-hand-scissors-o', 'fa', '&#xf257;'),
(345, 'fa-hand-spock-o', 'fa', '&#xf259;'),
(346, 'fa-hand-stop-o', 'fa', '&#xf256;'),
(347, 'fa-handshake-o', 'fa', '&#xf2b5;'),
(348, 'fa-hard-of-hearing', 'fa', '&#xf2a4;'),
(349, 'fa-hashtag', 'fa', '&#xf292;'),
(350, 'fa-hdd-o', 'fa', '&#xf0a0;'),
(351, 'fa-header', 'fa', '&#xf1dc;'),
(352, 'fa-headphones', 'fa', '&#xf025;'),
(353, 'fa-heart', 'fa', '&#xf004;'),
(354, 'fa-heart-o', 'fa', '&#xf08a;'),
(355, 'fa-heartbeat', 'fa', '&#xf21e;'),
(356, 'fa-history', 'fa', '&#xf1da;'),
(357, 'fa-home', 'fa', '&#xf015;'),
(358, 'fa-hospital-o', 'fa', '&#xf0f8;'),
(359, 'fa-hotel', 'fa', '&#xf236;'),
(360, 'fa-hourglass', 'fa', '&#xf254;'),
(361, 'fa-hourglass-1', 'fa', '&#xf251;'),
(362, 'fa-hourglass-2', 'fa', '&#xf252;'),
(363, 'fa-hourglass-3', 'fa', '&#xf253;'),
(364, 'fa-hourglass-end', 'fa', '&#xf253;'),
(365, 'fa-hourglass-half', 'fa', '&#xf252;'),
(366, 'fa-hourglass-o', 'fa', '&#xf250;'),
(367, 'fa-hourglass-start', 'fa', '&#xf251;'),
(368, 'fa-houzz', 'fa', '&#xf27c;'),
(369, 'fa-html5', 'fa', '&#xf13b;'),
(370, 'fa-i-cursor', 'fa', '&#xf246;'),
(371, 'fa-id-badge', 'fa', '&#xf2c1;'),
(372, 'fa-id-card', 'fa', '&#xf2c2;'),
(373, 'fa-id-card-o', 'fa', '&#xf2c3;'),
(374, 'fa-ils', 'fa', '&#xf20b;'),
(375, 'fa-image', 'fa', '&#xf03e;'),
(376, 'fa-imdb', 'fa', '&#xf2d8;'),
(377, 'fa-inbox', 'fa', '&#xf01c;'),
(378, 'fa-indent', 'fa', '&#xf03c;'),
(379, 'fa-industry', 'fa', '&#xf275;'),
(380, 'fa-info', 'fa', '&#xf129;'),
(381, 'fa-info-circle', 'fa', '&#xf05a;'),
(382, 'fa-inr', 'fa', '&#xf156;'),
(383, 'fa-instagram', 'fa', '&#xf16d;'),
(384, 'fa-institution', 'fa', '&#xf19c;'),
(385, 'fa-internet-explorer', 'fa', '&#xf26b;'),
(386, 'fa-intersex', 'fa', '&#xf224;'),
(387, 'fa-ioxhost', 'fa', '&#xf208;'),
(388, 'fa-italic', 'fa', '&#xf033;'),
(389, 'fa-joomla', 'fa', '&#xf1aa;'),
(390, 'fa-jpy', 'fa', '&#xf157;'),
(391, 'fa-jsfiddle', 'fa', '&#xf1cc;'),
(392, 'fa-key', 'fa', '&#xf084;'),
(393, 'fa-keyboard-o', 'fa', '&#xf11c;'),
(394, 'fa-krw', 'fa', '&#xf159;'),
(395, 'fa-language', 'fa', '&#xf1ab;'),
(396, 'fa-laptop', 'fa', '&#xf109;'),
(397, 'fa-lastfm', 'fa', '&#xf202;'),
(398, 'fa-lastfm-square', 'fa', '&#xf203;'),
(399, 'fa-leaf', 'fa', '&#xf06c;'),
(400, 'fa-leanpub', 'fa', '&#xf212;'),
(401, 'fa-legal', 'fa', '&#xf0e3;'),
(402, 'fa-lemon-o', 'fa', '&#xf094;'),
(403, 'fa-level-down', 'fa', '&#xf149;'),
(404, 'fa-level-up', 'fa', '&#xf148;'),
(405, 'fa-life-bouy', 'fa', '&#xf1cd;'),
(406, 'fa-life-buoy', 'fa', '&#xf1cd;'),
(407, 'fa-life-ring', 'fa', '&#xf1cd;'),
(408, 'fa-life-saver', 'fa', '&#xf1cd;'),
(409, 'fa-lightbulb-o', 'fa', '&#xf0eb;'),
(410, 'fa-line-chart', 'fa', '&#xf201;'),
(411, 'fa-link', 'fa', '&#xf0c1;'),
(412, 'fa-linkedin', 'fa', '&#xf0e1;'),
(413, 'fa-linkedin-square', 'fa', '&#xf08c;'),
(414, 'fa-linode', 'fa', '&#xf2b8;'),
(415, 'fa-linux', 'fa', '&#xf17c;'),
(416, 'fa-list', 'fa', '&#xf03a;'),
(417, 'fa-list-alt', 'fa', '&#xf022;'),
(418, 'fa-list-ol', 'fa', '&#xf0cb;'),
(419, 'fa-list-ul', 'fa', '&#xf0ca;'),
(420, 'fa-location-arrow', 'fa', '&#xf124;'),
(421, 'fa-lock', 'fa', '&#xf023;'),
(422, 'fa-long-arrow-down', 'fa', '&#xf175;'),
(423, 'fa-long-arrow-left', 'fa', '&#xf177;'),
(424, 'fa-long-arrow-right', 'fa', '&#xf178;'),
(425, 'fa-long-arrow-up', 'fa', '&#xf176;'),
(426, 'fa-low-vision', 'fa', '&#xf2a8;'),
(427, 'fa-magic', 'fa', '&#xf0d0;'),
(428, 'fa-magnet', 'fa', '&#xf076;'),
(429, 'fa-mail-forward', 'fa', '&#xf064;'),
(430, 'fa-mail-reply', 'fa', '&#xf112;'),
(431, 'fa-mail-reply-all', 'fa', '&#xf122;'),
(432, 'fa-male', 'fa', '&#xf183;'),
(433, 'fa-map', 'fa', '&#xf279;'),
(434, 'fa-map-marker', 'fa', '&#xf041;'),
(435, 'fa-map-o', 'fa', '&#xf278;'),
(436, 'fa-map-pin', 'fa', '&#xf276;'),
(437, 'fa-map-signs', 'fa', '&#xf277;'),
(438, 'fa-mars', 'fa', '&#xf222;'),
(439, 'fa-mars-double', 'fa', '&#xf227;'),
(440, 'fa-mars-stroke', 'fa', '&#xf229;'),
(441, 'fa-mars-stroke-h', 'fa', '&#xf22b;'),
(442, 'fa-mars-stroke-v', 'fa', '&#xf22a;'),
(443, 'fa-maxcdn', 'fa', '&#xf136;'),
(444, 'fa-meanpath', 'fa', '&#xf20c;'),
(445, 'fa-medium', 'fa', '&#xf23a;'),
(446, 'fa-medkit', 'fa', '&#xf0fa;'),
(447, 'fa-meetup', 'fa', '&#xf2e0;'),
(448, 'fa-meh-o', 'fa', '&#xf11a;'),
(449, 'fa-mercury', 'fa', '&#xf223;'),
(450, 'fa-microchip', 'fa', '&#xf2db;'),
(451, 'fa-microphone', 'fa', '&#xf130;'),
(452, 'fa-microphone-slash', 'fa', '&#xf131;'),
(453, 'fa-minus', 'fa', '&#xf068;'),
(454, 'fa-minus-circle', 'fa', '&#xf056;'),
(455, 'fa-minus-square', 'fa', '&#xf146;'),
(456, 'fa-minus-square-o', 'fa', '&#xf147;'),
(457, 'fa-mixcloud', 'fa', '&#xf289;'),
(458, 'fa-mobile', 'fa', '&#xf10b;'),
(459, 'fa-mobile-phone', 'fa', '&#xf10b;'),
(460, 'fa-modx', 'fa', '&#xf285;'),
(461, 'fa-money', 'fa', '&#xf0d6;'),
(462, 'fa-moon-o', 'fa', '&#xf186;'),
(463, 'fa-mortar-board', 'fa', '&#xf19d;'),
(464, 'fa-motorcycle', 'fa', '&#xf21c;'),
(465, 'fa-mouse-pointer', 'fa', '&#xf245;'),
(466, 'fa-music', 'fa', '&#xf001;'),
(467, 'fa-navicon', 'fa', '&#xf0c9;'),
(468, 'fa-neuter', 'fa', '&#xf22c;'),
(469, 'fa-newspaper-o', 'fa', '&#xf1ea;'),
(470, 'fa-object-group', 'fa', '&#xf247;'),
(471, 'fa-object-ungroup', 'fa', '&#xf248;'),
(472, 'fa-odnoklassniki', 'fa', '&#xf263;'),
(473, 'fa-odnoklassniki-square', 'fa', '&#xf264;'),
(474, 'fa-opencart', 'fa', '&#xf23d;'),
(475, 'fa-openid', 'fa', '&#xf19b;'),
(476, 'fa-opera', 'fa', '&#xf26a;'),
(477, 'fa-optin-monster', 'fa', '&#xf23c;'),
(478, 'fa-outdent', 'fa', '&#xf03b;'),
(479, 'fa-pagelines', 'fa', '&#xf18c;'),
(480, 'fa-paint-brush', 'fa', '&#xf1fc;'),
(481, 'fa-paper-plane', 'fa', '&#xf1d8;'),
(482, 'fa-paper-plane-o', 'fa', '&#xf1d9;'),
(483, 'fa-paperclip', 'fa', '&#xf0c6;'),
(484, 'fa-paragraph', 'fa', '&#xf1dd;'),
(485, 'fa-paste', 'fa', '&#xf0ea;'),
(486, 'fa-pause', 'fa', '&#xf04c;'),
(487, 'fa-pause-circle', 'fa', '&#xf28b;'),
(488, 'fa-pause-circle-o', 'fa', '&#xf28c;'),
(489, 'fa-paw', 'fa', '&#xf1b0;'),
(490, 'fa-paypal', 'fa', '&#xf1ed;'),
(491, 'fa-pencil', 'fa', '&#xf040;'),
(492, 'fa-pencil-square', 'fa', '&#xf14b;'),
(493, 'fa-pencil-square-o', 'fa', '&#xf044;'),
(494, 'fa-percent', 'fa', '&#xf295;'),
(495, 'fa-phone', 'fa', '&#xf095;'),
(496, 'fa-phone-square', 'fa', '&#xf098;'),
(497, 'fa-photo', 'fa', '&#xf03e;'),
(498, 'fa-picture-o', 'fa', '&#xf03e;'),
(499, 'fa-pie-chart', 'fa', '&#xf200;'),
(500, 'fa-pied-piper', 'fa', '&#xf2ae;'),
(501, 'fa-pied-piper-alt', 'fa', '&#xf1a8;'),
(502, 'fa-pied-piper-pp', 'fa', '&#xf1a7;'),
(503, 'fa-pinterest', 'fa', '&#xf0d2;'),
(504, 'fa-pinterest-p', 'fa', '&#xf231;'),
(505, 'fa-pinterest-square', 'fa', '&#xf0d3;'),
(506, 'fa-plane', 'fa', '&#xf072;'),
(507, 'fa-play', 'fa', '&#xf04b;'),
(508, 'fa-play-circle', 'fa', '&#xf144;'),
(509, 'fa-play-circle-o', 'fa', '&#xf01d;'),
(510, 'fa-plug', 'fa', '&#xf1e6;'),
(511, 'fa-plus', 'fa', '&#xf067;'),
(512, 'fa-plus-circle', 'fa', '&#xf055;'),
(513, 'fa-plus-square', 'fa', '&#xf0fe;'),
(514, 'fa-plus-square-o', 'fa', '&#xf196;'),
(515, 'fa-podcast', 'fa', '&#xf2ce;'),
(516, 'fa-power-off', 'fa', '&#xf011;'),
(517, 'fa-print', 'fa', '&#xf02f;'),
(518, 'fa-product-hunt', 'fa', '&#xf288;'),
(519, 'fa-puzzle-piece', 'fa', '&#xf12e;'),
(520, 'fa-qq', 'fa', '&#xf1d6;'),
(521, 'fa-qrcode', 'fa', '&#xf029;'),
(522, 'fa-question', 'fa', '&#xf128;'),
(523, 'fa-question-circle', 'fa', '&#xf059;'),
(524, 'fa-question-circle-o', 'fa', '&#xf29c;'),
(525, 'fa-quora', 'fa', '&#xf2c4;'),
(526, 'fa-quote-left', 'fa', '&#xf10d;'),
(527, 'fa-quote-right', 'fa', '&#xf10e;'),
(528, 'fa-ra', 'fa', '&#xf1d0;'),
(529, 'fa-random', 'fa', '&#xf074;'),
(530, 'fa-ravelry', 'fa', '&#xf2d9;'),
(531, 'fa-rebel', 'fa', '&#xf1d0;'),
(532, 'fa-recycle', 'fa', '&#xf1b8;'),
(533, 'fa-reddit', 'fa', '&#xf1a1;'),
(534, 'fa-reddit-alien', 'fa', '&#xf281;'),
(535, 'fa-reddit-square', 'fa', '&#xf1a2;'),
(536, 'fa-refresh', 'fa', '&#xf021;'),
(537, 'fa-registered', 'fa', '&#xf25d;'),
(538, 'fa-remove', 'fa', '&#xf00d;'),
(539, 'fa-renren', 'fa', '&#xf18b;'),
(540, 'fa-reorder', 'fa', '&#xf0c9;'),
(541, 'fa-repeat', 'fa', '&#xf01e;'),
(542, 'fa-reply', 'fa', '&#xf112;'),
(543, 'fa-reply-all', 'fa', '&#xf122;'),
(544, 'fa-resistance', 'fa', '&#xf1d0;'),
(545, 'fa-retweet', 'fa', '&#xf079;'),
(546, 'fa-rmb', 'fa', '&#xf157;'),
(547, 'fa-road', 'fa', '&#xf018;'),
(548, 'fa-rocket', 'fa', '&#xf135;'),
(549, 'fa-rotate-left', 'fa', '&#xf0e2;'),
(550, 'fa-rotate-right', 'fa', '&#xf01e;'),
(551, 'fa-rouble', 'fa', '&#xf158;'),
(552, 'fa-rss', 'fa', '&#xf09e;'),
(553, 'fa-rss-square', 'fa', '&#xf143;'),
(554, 'fa-rub', 'fa', '&#xf158;'),
(555, 'fa-ruble', 'fa', '&#xf158;'),
(556, 'fa-rupee', 'fa', '&#xf156;'),
(557, 'fa-s15', 'fa', '&#xf2cd;'),
(558, 'fa-safari', 'fa', '&#xf267;'),
(559, 'fa-save', 'fa', '&#xf0c7;'),
(560, 'fa-scissors', 'fa', '&#xf0c4;'),
(561, 'fa-scribd', 'fa', '&#xf28a;'),
(562, 'fa-search', 'fa', '&#xf002;'),
(563, 'fa-search-minus', 'fa', '&#xf010;'),
(564, 'fa-search-plus', 'fa', '&#xf00e;'),
(565, 'fa-sellsy', 'fa', '&#xf213;'),
(566, 'fa-send', 'fa', '&#xf1d8;'),
(567, 'fa-send-o', 'fa', '&#xf1d9;'),
(568, 'fa-server', 'fa', '&#xf233;'),
(569, 'fa-share', 'fa', '&#xf064;'),
(570, 'fa-share-alt', 'fa', '&#xf1e0;'),
(571, 'fa-share-alt-square', 'fa', '&#xf1e1;'),
(572, 'fa-share-square', 'fa', '&#xf14d;'),
(573, 'fa-share-square-o', 'fa', '&#xf045;'),
(574, 'fa-shekel', 'fa', '&#xf20b;'),
(575, 'fa-sheqel', 'fa', '&#xf20b;'),
(576, 'fa-shield', 'fa', '&#xf132;'),
(577, 'fa-ship', 'fa', '&#xf21a;'),
(578, 'fa-shirtsinbulk', 'fa', '&#xf214;'),
(579, 'fa-shopping-bag', 'fa', '&#xf290;'),
(580, 'fa-shopping-basket', 'fa', '&#xf291;'),
(581, 'fa-shopping-cart', 'fa', '&#xf07a;'),
(582, 'fa-shower', 'fa', '&#xf2cc;'),
(583, 'fa-sign-in', 'fa', '&#xf090;'),
(584, 'fa-sign-language', 'fa', '&#xf2a7;'),
(585, 'fa-sign-out', 'fa', '&#xf08b;'),
(586, 'fa-signal', 'fa', '&#xf012;'),
(587, 'fa-signing', 'fa', '&#xf2a7;'),
(588, 'fa-simplybuilt', 'fa', '&#xf215;'),
(589, 'fa-sitemap', 'fa', '&#xf0e8;'),
(590, 'fa-skyatlas', 'fa', '&#xf216;'),
(591, 'fa-skype', 'fa', '&#xf17e;'),
(592, 'fa-slack', 'fa', '&#xf198;'),
(593, 'fa-sliders', 'fa', '&#xf1de;'),
(594, 'fa-slideshare', 'fa', '&#xf1e7;'),
(595, 'fa-smile-o', 'fa', '&#xf118;'),
(596, 'fa-snapchat', 'fa', '&#xf2ab;'),
(597, 'fa-snapchat-ghost', 'fa', '&#xf2ac;'),
(598, 'fa-snapchat-square', 'fa', '&#xf2ad;'),
(599, 'fa-snowflake-o', 'fa', '&#xf2dc;'),
(600, 'fa-soccer-ball-o', 'fa', '&#xf1e3;'),
(601, 'fa-sort', 'fa', '&#xf0dc;'),
(602, 'fa-sort-alpha-asc', 'fa', '&#xf15d;'),
(603, 'fa-sort-alpha-desc', 'fa', '&#xf15e;'),
(604, 'fa-sort-amount-asc', 'fa', '&#xf160;'),
(605, 'fa-sort-amount-desc', 'fa', '&#xf161;'),
(606, 'fa-sort-asc', 'fa', '&#xf0de;'),
(607, 'fa-sort-desc', 'fa', '&#xf0dd;'),
(608, 'fa-sort-down', 'fa', '&#xf0dd;'),
(609, 'fa-sort-numeric-asc', 'fa', '&#xf162;'),
(610, 'fa-sort-numeric-desc', 'fa', '&#xf163;'),
(611, 'fa-sort-up', 'fa', '&#xf0de;'),
(612, 'fa-soundcloud', 'fa', '&#xf1be;'),
(613, 'fa-space-shuttle', 'fa', '&#xf197;'),
(614, 'fa-spinner', 'fa', '&#xf110;'),
(615, 'fa-spoon', 'fa', '&#xf1b1;'),
(616, 'fa-spotify', 'fa', '&#xf1bc;'),
(617, 'fa-square', 'fa', '&#xf0c8;'),
(618, 'fa-square-o', 'fa', '&#xf096;'),
(619, 'fa-stack-exchange', 'fa', '&#xf18d;'),
(620, 'fa-stack-overflow', 'fa', '&#xf16c;'),
(621, 'fa-star', 'fa', '&#xf005;'),
(622, 'fa-star-half', 'fa', '&#xf089;'),
(623, 'fa-star-half-empty', 'fa', '&#xf123;'),
(624, 'fa-star-half-full', 'fa', '&#xf123;'),
(625, 'fa-star-half-o', 'fa', '&#xf123;'),
(626, 'fa-star-o', 'fa', '&#xf006;'),
(627, 'fa-steam', 'fa', '&#xf1b6;'),
(628, 'fa-steam-square', 'fa', '&#xf1b7;'),
(629, 'fa-step-backward', 'fa', '&#xf048;'),
(630, 'fa-step-forward', 'fa', '&#xf051;'),
(631, 'fa-stethoscope', 'fa', '&#xf0f1;'),
(632, 'fa-sticky-note', 'fa', '&#xf249;'),
(633, 'fa-sticky-note-o', 'fa', '&#xf24a;'),
(634, 'fa-stop', 'fa', '&#xf04d;'),
(635, 'fa-stop-circle', 'fa', '&#xf28d;'),
(636, 'fa-stop-circle-o', 'fa', '&#xf28e;'),
(637, 'fa-street-view', 'fa', '&#xf21d;'),
(638, 'fa-strikethrough', 'fa', '&#xf0cc;'),
(639, 'fa-stumbleupon', 'fa', '&#xf1a4;'),
(640, 'fa-stumbleupon-circle', 'fa', '&#xf1a3;'),
(641, 'fa-subscript', 'fa', '&#xf12c;'),
(642, 'fa-subway', 'fa', '&#xf239;'),
(643, 'fa-suitcase', 'fa', '&#xf0f2;'),
(644, 'fa-sun-o', 'fa', '&#xf185;'),
(645, 'fa-superpowers', 'fa', '&#xf2dd;'),
(646, 'fa-superscript', 'fa', '&#xf12b;'),
(647, 'fa-support', 'fa', '&#xf1cd;'),
(648, 'fa-table', 'fa', '&#xf0ce;'),
(649, 'fa-tablet', 'fa', '&#xf10a;'),
(650, 'fa-tachometer', 'fa', '&#xf0e4;'),
(651, 'fa-tag', 'fa', '&#xf02b;'),
(652, 'fa-tags', 'fa', '&#xf02c;'),
(653, 'fa-tasks', 'fa', '&#xf0ae;'),
(654, 'fa-taxi', 'fa', '&#xf1ba;'),
(655, 'fa-telegram', 'fa', '&#xf2c6;'),
(656, 'fa-television', 'fa', '&#xf26c;'),
(657, 'fa-tencent-weibo', 'fa', '&#xf1d5;'),
(658, 'fa-terminal', 'fa', '&#xf120;'),
(659, 'fa-text-height', 'fa', '&#xf034;'),
(660, 'fa-text-width', 'fa', '&#xf035;'),
(661, 'fa-th', 'fa', '&#xf00a;'),
(662, 'fa-th-large', 'fa', '&#xf009;'),
(663, 'fa-th-list', 'fa', '&#xf00b;'),
(664, 'fa-themeisle', 'fa', '&#xf2b2;'),
(665, 'fa-thermometer', 'fa', '&#xf2c7;'),
(666, 'fa-thermometer-0', 'fa', '&#xf2cb;'),
(667, 'fa-thermometer-1', 'fa', '&#xf2ca;'),
(668, 'fa-thermometer-2', 'fa', '&#xf2c9;'),
(669, 'fa-thermometer-3', 'fa', '&#xf2c8;'),
(670, 'fa-thermometer-4', 'fa', '&#xf2c7;'),
(671, 'fa-thermometer-empty', 'fa', '&#xf2cb;'),
(672, 'fa-thermometer-full', 'fa', '&#xf2c7;'),
(673, 'fa-thermometer-half', 'fa', '&#xf2c9;'),
(674, 'fa-thermometer-quarter', 'fa', '&#xf2ca;'),
(675, 'fa-thermometer-three-quarters', 'fa', '&#xf2c8;'),
(676, 'fa-thumb-tack', 'fa', '&#xf08d;'),
(677, 'fa-thumbs-down', 'fa', '&#xf165;'),
(678, 'fa-thumbs-o-down', 'fa', '&#xf088;'),
(679, 'fa-thumbs-o-up', 'fa', '&#xf087;'),
(680, 'fa-thumbs-up', 'fa', '&#xf164;'),
(681, 'fa-ticket', 'fa', '&#xf145;'),
(682, 'fa-times', 'fa', '&#xf00d;'),
(683, 'fa-times-circle', 'fa', '&#xf057;'),
(684, 'fa-times-circle-o', 'fa', '&#xf05c;'),
(685, 'fa-times-rectangle', 'fa', '&#xf2d3;'),
(686, 'fa-times-rectangle-o', 'fa', '&#xf2d4;'),
(687, 'fa-tint', 'fa', '&#xf043;'),
(688, 'fa-toggle-down', 'fa', '&#xf150;'),
(689, 'fa-toggle-left', 'fa', '&#xf191;'),
(690, 'fa-toggle-off', 'fa', '&#xf204;'),
(691, 'fa-toggle-on', 'fa', '&#xf205;'),
(692, 'fa-toggle-right', 'fa', '&#xf152;'),
(693, 'fa-toggle-up', 'fa', '&#xf151;'),
(694, 'fa-trademark', 'fa', '&#xf25c;'),
(695, 'fa-train', 'fa', '&#xf238;'),
(696, 'fa-transgender', 'fa', '&#xf224;'),
(697, 'fa-transgender-alt', 'fa', '&#xf225;'),
(698, 'fa-trash', 'fa', '&#xf1f8;'),
(699, 'fa-trash-o', 'fa', '&#xf014;'),
(700, 'fa-tree', 'fa', '&#xf1bb;'),
(701, 'fa-trello', 'fa', '&#xf181;'),
(702, 'fa-tripadvisor', 'fa', '&#xf262;'),
(703, 'fa-trophy', 'fa', '&#xf091;'),
(704, 'fa-truck', 'fa', '&#xf0d1;'),
(705, 'fa-try', 'fa', '&#xf195;'),
(706, 'fa-tty', 'fa', '&#xf1e4;'),
(707, 'fa-tumblr', 'fa', '&#xf173;'),
(708, 'fa-tumblr-square', 'fa', '&#xf174;'),
(709, 'fa-turkish-lira', 'fa', '&#xf195;'),
(710, 'fa-tv', 'fa', '&#xf26c;'),
(711, 'fa-twitch', 'fa', '&#xf1e8;'),
(712, 'fa-twitter', 'fa', '&#xf099;'),
(713, 'fa-twitter-square', 'fa', '&#xf081;'),
(714, 'fa-umbrella', 'fa', '&#xf0e9;'),
(715, 'fa-underline', 'fa', '&#xf0cd;'),
(716, 'fa-undo', 'fa', '&#xf0e2;'),
(717, 'fa-universal-access', 'fa', '&#xf29a;'),
(718, 'fa-university', 'fa', '&#xf19c;'),
(719, 'fa-unlink', 'fa', '&#xf127;'),
(720, 'fa-unlock', 'fa', '&#xf09c;'),
(721, 'fa-unlock-alt', 'fa', '&#xf13e;'),
(722, 'fa-unsorted', 'fa', '&#xf0dc;'),
(723, 'fa-upload', 'fa', '&#xf093;'),
(724, 'fa-usb', 'fa', '&#xf287;'),
(725, 'fa-usd', 'fa', '&#xf155;'),
(726, 'fa-user', 'fa', '&#xf007;'),
(727, 'fa-user-circle', 'fa', '&#xf2bd;'),
(728, 'fa-user-circle-o', 'fa', '&#xf2be;'),
(729, 'fa-user-md', 'fa', '&#xf0f0;'),
(730, 'fa-user-o', 'fa', '&#xf2c0;'),
(731, 'fa-user-plus', 'fa', '&#xf234;'),
(732, 'fa-user-secret', 'fa', '&#xf21b;'),
(733, 'fa-user-times', 'fa', '&#xf235;'),
(734, 'fa-users', 'fa', '&#xf0c0;'),
(735, 'fa-vcard', 'fa', '&#xf2bb;'),
(736, 'fa-vcard-o', 'fa', '&#xf2bc;'),
(737, 'fa-venus', 'fa', '&#xf221;'),
(738, 'fa-venus-double', 'fa', '&#xf226;'),
(739, 'fa-venus-mars', 'fa', '&#xf228;'),
(740, 'fa-viacoin', 'fa', '&#xf237;'),
(741, 'fa-viadeo', 'fa', '&#xf2a9;'),
(742, 'fa-viadeo-square', 'fa', '&#xf2aa;'),
(743, 'fa-video-camera', 'fa', '&#xf03d;'),
(744, 'fa-vimeo', 'fa', '&#xf27d;'),
(745, 'fa-vimeo-square', 'fa', '&#xf194;'),
(746, 'fa-vine', 'fa', '&#xf1ca;'),
(747, 'fa-vk', 'fa', '&#xf189;'),
(748, 'fa-volume-control-phone', 'fa', '&#xf2a0;'),
(749, 'fa-volume-down', 'fa', '&#xf027;'),
(750, 'fa-volume-off', 'fa', '&#xf026;'),
(751, 'fa-volume-up', 'fa', '&#xf028;'),
(752, 'fa-warning', 'fa', '&#xf071;'),
(753, 'fa-wechat', 'fa', '&#xf1d7;'),
(754, 'fa-weibo', 'fa', '&#xf18a;'),
(755, 'fa-weixin', 'fa', '&#xf1d7;'),
(756, 'fa-whatsapp', 'fa', '&#xf232;'),
(757, 'fa-wheelchair', 'fa', '&#xf193;'),
(758, 'fa-wheelchair-alt', 'fa', '&#xf29b;'),
(759, 'fa-wifi', 'fa', '&#xf1eb;'),
(760, 'fa-wikipedia-w', 'fa', '&#xf266;'),
(761, 'fa-window-close', 'fa', '&#xf2d3;'),
(762, 'fa-window-close-o', 'fa', '&#xf2d4;'),
(763, 'fa-window-maximize', 'fa', '&#xf2d0;'),
(764, 'fa-window-minimize', 'fa', '&#xf2d1;'),
(765, 'fa-window-restore', 'fa', '&#xf2d2;'),
(766, 'fa-windows', 'fa', '&#xf17a;'),
(767, 'fa-won', 'fa', '&#xf159;'),
(768, 'fa-wordpress', 'fa', '&#xf19a;'),
(769, 'fa-wpbeginner', 'fa', '&#xf297;'),
(770, 'fa-wpexplorer', 'fa', '&#xf2de;'),
(771, 'fa-wpforms', 'fa', '&#xf298;'),
(772, 'fa-wrench', 'fa', '&#xf0ad;'),
(773, 'fa-xing', 'fa', '&#xf168;'),
(774, 'fa-xing-square', 'fa', '&#xf169;'),
(775, 'fa-y-combinator', 'fa', '&#xf23b;'),
(776, 'fa-y-combinator-square', 'fa', '&#xf1d4;'),
(777, 'fa-yahoo', 'fa', '&#xf19e;'),
(778, 'fa-yc', 'fa', '&#xf23b;'),
(779, 'fa-yc-square', 'fa', '&#xf1d4;'),
(780, 'fa-yelp', 'fa', '&#xf1e9;'),
(781, 'fa-yen', 'fa', '&#xf157;'),
(782, 'fa-yoast', 'fa', '&#xf2b1;'),
(783, 'fa-youtube', 'fa', '&#xf167;'),
(784, 'fa-youtube-play', 'fa', '&#xf16a;'),
(785, 'fa-youtube-square', 'fa', '&#xf166;');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `title`, `icon`, `url`, `is_active`) VALUES
(1, 'Dashboard', 'fa fa-tachometer', 'dashboard', 1),
(2, 'Pengguna', 'fa fa-users', 'users', 1),
(4, 'Menu', 'fa fa-navicon', 'menu', 1),
(5, 'Pengaturan', 'fa fa-gear', 'pengaturan', 1),
(6, 'Hak Akses', 'fa fa-shield', 'role', 1),
(7, 'Hak Akses Menu', 'fa fa-th-list', 'access', 1),
(14, 'Keluar', 'fa fa-sign-out', 'auth/logout', 1),
(15, 'report user', 'fa fa-adjust', 'report', 1),
(16, 'Billing', 'fa fa-apple', 'ade', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `nama_sistem` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `nama_sistem`) VALUES
(1, 'M-Base 123');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'Admin'),
(2, 'Petugas'),
(4, 'Unassigned');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(2, 'system_name', 'AUTONOTIF'),
(3, 'system_title', 'Dashboard Whatsapp Gateway'),
(4, 'system_email', 'ranggaanggrawan@gmail.com'),
(7, 'purchase_code', NULL),
(12, 'slogan', 'A course based video CMS'),
(19, 'author', 'ARDESIGN'),
(21, 'website_description', 'Layanan Whatsapp Gateway Terbaik'),
(22, 'website_keywords', 'LMS,Learning Management System'),
(23, 'footer_text', 'ARDESIGN'),
(24, 'footer_link', 'https://ardesign.web.id/'),
(30, 'version', '6.0'),
(31, 'server', 'http://localhost:3000');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `nama` varchar(128) NOT NULL,
  `telepon` varchar(16) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `linkedin` varchar(100) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `perusahaan` varchar(100) DEFAULT NULL,
  `negara` varchar(100) DEFAULT NULL,
  `job` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_hint` varchar(100) DEFAULT NULL,
  `img` varchar(128) NOT NULL,
  `tentang` text DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `otp` varchar(10) NOT NULL,
  `status_aktif` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `nama`, `telepon`, `email`, `twitter`, `facebook`, `instagram`, `linkedin`, `alamat`, `perusahaan`, `negara`, `job`, `password`, `password_hint`, `img`, `tentang`, `date_created`, `otp`, `status_aktif`) VALUES
(4, 1, 'Ade Yaser Arafat', '08595923132315 v', 'admin@admin.com', '', 'https://www.facebook.com/ade.yaser/v', 'https://www.instagram.com/yaser_ade/v', 'https://www.linkedin.com/in/ade-yaser-arafat-2310a1128/v', 'KP.Rawa bamabu Rt.003 /Rw 003 KP Bekasi v1', 'ILCS v1', 'Indonesia', 'Enginer Developer v1', '$2y$10$TENVoQY9HwB.fitIv8kYX.fQjxVB3UtikYIy.Crnm9PZRCEtQY6fm', 'admin', 'Image20231223221459.png', 'I am a web, mobile and desktop programming developer I have more than 3 years experience in web programming Likewise for mobile and desktop I\'m used to using git lab or git hub, and it really helps me in collaborating and coding ', '2023-11-06 02:46:33', '', '1'),
(8, 2, 'petugas', '', 'petugas@petugas.com', '', '', '', '', '', '', '', '', '$2y$10$oAdBYej2nXsqqoI8aEXL/OWlsguZCf0Cuh7tV.OMTXUV/QCAMfdAa', '', 'default.png', '', '2023-11-01 09:58:11', '', NULL),
(21, 2, 'ADE YASER', '085923132315', 'adeyaserarafat1@gmail.com', '', 'https://www.facebook.com/ade.yaser/v1', 'https://www.instagram.com/yaser_ade/v1', 'https://www.linkedin.com/in/ade-yaser-arafat-2310a1128/v1', 'jl.mawar 1 kalibaru bekasi', 'PT.indonesia', 'Indonesia', 'Enginer Developer', '$2y$10$O3LKmcxYmFgqB0fQvk.0Huof9CORNIAiT8FMcW5CBK3WTb67nOwXu', '123456', 'ronaldo.jpg', 'hanya manusia biasa', '2023-11-26 10:00:01', '480601', '1'),
(22, 0, 'aw', NULL, 'anggi.batavianet@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '$2y$10$XtZJcGDttBoIs16aE7MPiuDEQW1Vu4wEF50YUGFOapIxj2DAQMVI.', '123456', 'default.png', NULL, '2023-12-04 02:10:29', '4831029', NULL),
(28, 0, 'Ade Yaser Arafat', NULL, 'ade.yaser19@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '$2y$10$qbPcZPvPm0NZ9cD612zjVe1c7cRMxlQYVZ3XfbVse4Z2Gk0A9a6dG', '12132324325', 'default.png', NULL, '2023-12-17 10:53:19', '4861019', NULL),
(29, 0, 'adey', NULL, 'adeyaserarafat@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '$2y$10$tUbMWT5vKhnL/HFazJa93OgW/UNCrV.Cmi0NNRn5Sgjm.WhwOtp3G', 'Bsihalohalo', 'default.png', NULL, '2023-12-17 16:13:39', '2810439', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(3, 1, 4),
(4, 3, 1),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(11, 1, 2),
(20, 2, 1),
(21, 1, 13),
(22, 2, 13),
(23, 1, 14),
(24, 2, 14),
(0, 1, 16);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `icon`
--
ALTER TABLE `icon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `icon`
--
ALTER TABLE `icon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=786;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
